"use strict";

module.exports = function (_ref) {
  var env = _ref.env;
  return {
    defaultConnection: 'default',
    connections: {
      "default": {
        connector: 'bookshelf',
        settings: {
          client: "mysql",
          host: env('DATABASE_HOST', 'mysql.rackhost.hu'),
          port: env('DATABASE_PORT', 3306),
          database: env('DATABASE_NAME', 'c18828tremolo'),
          username: env('DATABASE_USERNAME', 'c18828lulity@localhost'),
          password: env('DATABASE_PASSWORD', 'tremolo21')
        },
        options: {
          useNullAsDefault: true
        }
      }
    }
  };
};
