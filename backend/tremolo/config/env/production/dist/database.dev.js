"use strict";

module.exports = function (_ref) {
  var env = _ref.env;
  return {
    defaultConnection: 'default',
    connections: {
      "default": {
        connector: 'bookshelf',
        settings: {
          client: "mysql",
          host: env('DATABASE_HOST', 'mysql.rackhost.hu	'),
          port: env('DATABASE_PORT', 22),
          database: env('DATABASE_NAME', 'c18828tremolo'),
          username: env('DATABASE_USERNAME', 'c18828lule'),
          password: env('DATABASE_PASSWORD', 'tremolo2021')
        },
        options: {
          useNullAsDefault: true
        }
      }
    }
  };
};