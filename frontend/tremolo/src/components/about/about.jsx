import React, { Component } from "react";
import "./about.scss";
import ReactMarkdown from "react-markdown";
import contentService from "../../services/contentService";
import { CircularProgress } from '@material-ui/core';

export default class About extends Component {
  constructor(props){
    super(props)
    this.state = {
      about: []
    }
  }

  componentDidMount() {
    this.getData()
  }

  async getData() {
    await contentService.getAbout().then(response => {
      this.setState({
        about: response.data
      })
    })
  }

  render() {
    if(this.state.about.length > 0){
      return (
        <div className="about-container">
          <div className="title-container">
            <div className="title">PROJECT PRESENTATION</div>
          </div>
          <div className="text">
            <ReactMarkdown>
            {this.state.about[0].About}
            </ReactMarkdown>
          </div>
        </div>
      );
    } else {
      return (
        <div  className="about-container" style={{height: '100vh'}}>
           <div className="title-container">
            <div className="title">PROJECT PRESENTATION</div>
          </div>
          <CircularProgress size={100}/>
        </div>
      );
    }
    
  }
}
