import React, { Component } from 'react'
import './news.scss'
import { NavLink } from "react-router-dom";
import contentService from "../../services/contentService";
import Button from "@material-ui/core/Button";
import TextTruncate from 'react-text-truncate';

export default class LatestNews extends Component {
    constructor(props) {
        super(props)
        this.state = {
            blogs: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await contentService.getBlogs().then(response => {
            let items = response.data.slice(response.data.length - 3, response.data.length).map(blog => {
                return (
                    <div className="news" key="blog.id">
                        <div className="news-title">
                            {blog.Title}
                        </div>
                        <div className="news-date">
                            2021.06.01
                        </div>
                        <div className="news-image">
                            <img src={blog.Header} alt="" />
                        </div>
                        <div className="news-text">
                            <TextTruncate
                                line={4}
                                element="span"
                                truncateText="…"
                                text={blog.Post}
                            />
                        </div>
                        <NavLink to={{ pathname: "/news/" + blog.Title, state: { blog } }} >
                            <Button variant="contained" color="primary" className="button">Read More</Button>
                        </NavLink>
                    </div>
                )
            });
            this.setState({
                blogs: items

            })
        })
    }
    render() {
        return (
            <div className="latest-news">
                <div className="latest-news-title">
                    LATEST NEWS
                </div>
                {this.state.blogs}
            </div>
        )
    }
}
