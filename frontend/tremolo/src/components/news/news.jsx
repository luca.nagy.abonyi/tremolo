import React, { Component } from 'react';
import './news.scss';
import contentService from "../../services/contentService";
import Button from "@material-ui/core/Button";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { CircularProgress } from '@material-ui/core';
import TextTruncate from 'react-text-truncate';

class News extends Component {
    constructor(props) {
        super(props)
        this.state = {
            blogs: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await contentService.getBlogs().then(response => {
            this.setState({
                blogs: response.data
            })
        })
    }

    render() {
        if (this.state.blogs.length > 0) {
            return (
                <div className="news-container">
                    <div className="title">
                        <div className="title-container">
                            <div className="title">
                                NEWS
                            </div>
                        </div>
                    </div>
                    <div className="news-list">
                    {this.state.blogs.map(blog =>
                        <div className="news" key={blog.id}>
                            <div className="news-title">
                                {blog.Title}
                            </div>
                            <div className="news-date">
                                2021.06.01
                            </div>
                            <div className="news-image">
                                <img src={blog.Header} alt="" />
                            </div>
                            <div className="news-text">
                                <TextTruncate
                                    line={4}
                                    element="span"
                                    truncateText="…"
                                    text={blog.Post}
                                />
                            </div>
                            <NavLink to={{ pathname: "/news/" + blog.Title, state: { blog } }} >
                                <Button variant="contained" color="primary" className="button">Read More</Button>
                            </NavLink>
                        </div>
                    )}
                    </div>
                </div>
            )
        } else {
            return (
                <div className='partners-container'>
                    <div className="title-container">
                            <div className="title">
                                NEWS
                            </div>
                        </div>
                    {/*<CircularProgress size={100} /> */}
                    
                </div>
            )
        }
    }
}
export default withRouter(News)