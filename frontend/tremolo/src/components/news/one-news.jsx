import React, { Component } from 'react'
import './news.scss'
import ReactMarkdown from "react-markdown";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { NavLink } from "react-router-dom";
import LatestNews from './latest-news';

export default class OneNews extends Component {

    render() {
        return (
            <div>
                <div className="one-news-container">
                    <div >
                        <NavLink to={'/news'} className="back" >
                            <ArrowBackIcon />
                            <div>NEWS</div>
                        </NavLink>
                    </div>
                    <div className="one-news">
                        <div className="date">
                            2021.06.01.
                        </div>
                        <div className="title">
                            {this.props.location.state.blog.Title}
                        </div>
                        <div className="text">
                            <ReactMarkdown>
                                {this.props.location.state.blog.Post}
                            </ReactMarkdown>
                        </div>
                    </div>
                </div>
                <LatestNews />
            </div>
        )


    }
}
