import React, { Component } from "react";
import "./homepage.scss";
import Button from "@material-ui/core/Button";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";
import Artist from "../artists/artist";
import Contact from "../contact/contact";
import contentService from "../../services/contentService";
import { CircularProgress } from '@material-ui/core';
import LatestNews from "../news/latest-news";

class Homepage extends Component {
  activities_icons = [
    'https://i.ibb.co/hCwT0NH/electric-guitar-1.png',
    'https://i.ibb.co/JsLcX60/accordion.png',
    'https://i.ibb.co/vBRhryc/keyboard.png',
    'https://i.ibb.co/zRrNf5K/Eszk-z-1.png'
  ]
  constructor(props) {
    super(props);
    this.state = {
      scroll: false,
      hover: false,
      data: [],
      partners: [],
      activities: [],
      artists: []
    };
  }
  setHover(value) {
    this.setState({
      hover: value,
    });
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll.bind(this), true);
    this.getData()
  }

  async getData() {
    await contentService.getHomepage().then(response => {
      this.setState({
        data: response.data
      })
    })

    await contentService.getPartners().then(response => {
      this.setState({
        partners: response.data
      })
    })

    await contentService.getActivities().then(response => {
      this.setState({
        activities: response.data
      })
    })


    await contentService.getArtists().then(response => {
      this.setState({
        artists: response.data
      })
    })
  }

  handleScroll() {
    if (window.pageYOffset > 100) {
      this.setState({
        scroll: true,
      });
    } else {
      this.setState({
        scroll: false,
      });
    }
  }

  toArtist(name){
    this.props.history.push('artists/'+name)
  }
  
  render() {

    if (this.state.data.length > 0) {
      return (
        <div className="homepage-container">
          <div className="landing">
            <div className="texts">
              <div
                className={"site-name"}
              >
                <img
                  src="https://i.ibb.co/WVvq4Pc/Tremolo-logo-txt-5.png"
                  alt=""
                />
              </div>
              <div className="short-desc">
                <img src="https://i.ibb.co/bPs116D/tremolo-felirat.png" alt="" />
              </div>
              <div className="eu-logo">
                <img src="https://i.ibb.co/dPM0Trz/logosbeneficairescreativeeuropeleft-en.jpg" alt="" />
              </div>
              <div className="buttons">
                <Button variant="contained" id="projectbutton">
                  <NavLink to="./about">Project</NavLink>
                </Button>
                <Button variant="contained" color="primary">
                  <NavLink to="./partners"> Partners</NavLink>
                </Button>
              </div>
            </div>
            <div
              className={this.state.scroll ? "navbar-icon-scroll" : "navbar-icon"}
            >
              <img
                src="https://i.ibb.co/kGvwLwn/ezgif-com-gif-maker-2.gif"
                alt=""
              />
            </div>
          </div>
          <div className="about">
            <div className="title">About</div>
            <div className="about-primary">
              {this.state.data[2].text}
            </div>
            <div className="about-secondary">
              {this.state.data[0].text}

            </div>
            <div className="about-secondary-mobile">
              {this.state.data[1].text}

            </div>
            <div className="about-button">
              <Button variant="contained" id="about-button">
                <NavLink to="./about">Read more</NavLink>
              </Button>
            </div>
          </div>

          <div className="partners-container">
            <div className="partners-title">
              <div className="title">Meet our partners!</div>
            </div>
            <div className="partners">
              {
                this.state.partners.map(partner =>
                  <div className="partner" key={partner.id}>
                    <NavLink to="./partners">
                      <img
                        src={partner.url}
                        alt={partner.id}
                      />
                    </NavLink>
                  </div>
                )
              }
            </div>
          </div>

          <div className="artists-container">
            <div className="title artist-title">ARTISTS</div>
            <div className="artists">
              {
                this.state.artists.map(artist =>
                  
                  <Artist
                    url={artist.picture_link}
                    name={artist.name}
                    id={artist.id}
                    artist={artist}
                  />
                )
              }


            </div>
          </div>
          <div className="activities-container">
            <div className="title">Our Activities</div>

            <div className="activities">
              {
                this.state.activities.slice(0, 4).map((activity, index) =>
                  <div className="activity" key={activity.id}>
                    <div>
                      <img
                        src={this.activities_icons[index]}
                        alt=""
                      />
                    </div>
                    <div className="activity-details">
                      <div className="activity-title">
                        {activity.Title}
                      </div>
                      <div className="activity-desc">
                        {activity.Short_description}
                      </div>
                      <div className="activity-button">
                        <Button variant="contained" color="primary">
                          <NavLink to="./activities">Read more</NavLink>
                        </Button>

                      </div>
                    </div>
                  </div>
                )
              }

            </div>
            <div className="show-all-button" style={{ textAlign: 'center', margin: '20px 0' }}>
              <Button variant="contained" color="primary" >
                <NavLink to="./activities">SHOW ALL</NavLink>
              </Button>
            </div>

          </div>
          <div className="contact-container">
            <Contact></Contact>
          </div>
{/*           <LatestNews />
 */}
        </div>
      );
    } else {
      return (
        <div className="circular-container">
          <CircularProgress />
        </div>
      )
    }
  }
}

export default withRouter(Homepage);
