import React, { Component } from "react";
import "./footer.scss";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";
import { NavLink } from "react-router-dom";
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import { withRouter } from "react-router-dom";

class Footer extends Component {
  render() {
    return (
      <div className="footer-container">
        <div className="row">
          <div className="col">
            <img src="https://i.ibb.co/WVvq4Pc/Tremolo-logo-txt-5.png" alt="" />

          </div>
          <div className="social-media-icons">
            <div>
              <a href="https://www.facebook.com/TremoloProject" target="_blank" rel="noreferrer">
                <FacebookIcon></FacebookIcon>
              </a>
              <a
                href="https://www.instagram.com/tremolo.project/?fbclid=IwAR3NNr5VEC8y_z-UI2w0lSLio4lIzhhQjUnd-suFBD1tOVYtohHhxmABlA8"
                target="_blank"
                rel="noreferrer"
              >
                <InstagramIcon></InstagramIcon>
              </a>
            </div>
          </div>
          <div className="eu-logo-container">
            <img src="https://i.ibb.co/dPM0Trz/logosbeneficairescreativeeuropeleft-en.jpg" alt="" />
          </div>
        </div>
        <div className="row">
          <div className="col">
          
          </div>
          <div className="col"></div>

          <div className="col">
            <div className="menu-row">
              <div className="menu">
                <NavLink to="/about">Project Presentation </NavLink>
              </div>
              <div>|</div>
              <div className="menu">
                <NavLink to="/partners">Partners </NavLink>
              </div>
              <div>|</div>
              <div className="menu">
                <NavLink to="/activities">Activities</NavLink>
              </div>
              <div>|</div>
              <div className="menu">
                <NavLink to="/artists">Our Artists</NavLink>{" "}
              </div>
              <div>|</div>
              <div className="menu">
                <NavLink to="/news">News</NavLink>
              </div>
              <div>|</div>
              <div className="menu">
                <NavLink to="/calls">Open Calls</NavLink>
              </div>
              <div>|</div>
              <div className="menu">
                <NavLink to="/contact">Contact</NavLink>
              </div>
            </div>
          </div>

        </div>
        <div className="row">
          <div className="col"></div>
          <div className="col"></div>
          <div className="col">
          <div className="email">
            <AlternateEmailIcon /> getintouch@tremoloproject.eu
          </div>

          </div>
        </div>
       {/*  <div className="impressum">
          <NavLink to="/impress">Impressum</NavLink> | Copyright
        </div> */}
      </div>
    );
  }
}
export default withRouter(Footer);
