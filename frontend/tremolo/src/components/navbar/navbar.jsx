import React, { Component } from "react";
import "./navbar.scss";
import { NavLink } from "react-router-dom";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";
import Button from "@material-ui/core/Button";
import { withRouter } from "react-router-dom";
import navbarLogo from "../../assets/ezgif.com-gif-maker (2).gif";
import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scroll: false,
      homepage: true,
      mobileMenu: false,
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll.bind(this), true);
    this.isHomepage();
  }
  componentWillReceiveProps() {
    this.isHomepage();
  }

  isHomepage() {
    if (this.props.history.location.pathname === "/") {
      this.setState({
        homepage: true,
      });
    } else {
      this.setState({
        homepage: false,
      });
    }
  }

  handleScroll() {
    if (window.pageYOffset > 100) {
      this.setState({
        scroll: true,
      });
    } else {
      this.setState({
        scroll: false,
      });
    }
  }

  toHomepage() {
    this.setState(
      {
        homepage: false,
      },
      () => {
        this.props.history.push("/");
      }
    );
  }

  setMobileMenu(value) {
    this.setState({
      mobileMenu: value,
    });
  }

  closeNav() {
    this.setState({
      mobileMenu: false,
    });
  }

  render() {
    return (
      <div>
        <div className="mobile-menu">
          <div className="social-icons" style={{ width: '100px' }}>
            <a href="https://www.facebook.com/TremoloProject" target="_blank" rel="noreferrer">
              <FacebookIcon></FacebookIcon>
            </a>
            <a
              href="https://www.instagram.com/tremolo.project/?fbclid=IwAR3NNr5VEC8y_z-UI2w0lSLio4lIzhhQjUnd-suFBD1tOVYtohHhxmABlA8"
              target="_blank"
              rel="noreferrer"
            >
              <InstagramIcon></InstagramIcon>
            </a>
          </div>
          <div className="logo" style={{ width: '100px' }}>
            <NavLink to="/">
              <img src={navbarLogo} alt='logo'/>
            </NavLink>
          </div>
          <div className="menu-logo" style={{ width: '100px' }}>
            {this.state.mobileMenu ? (
              <CloseIcon
                onClick={() => this.setMobileMenu(!this.state.mobileMenu)}
              />
            ) : (
              <MenuIcon
                onClick={() => this.setMobileMenu(!this.state.mobileMenu)}
              />
            )}
          </div>
        </div>
        {this.state.mobileMenu ? (
          <div className="mobile-menu-container">
            <div className="mobile-menu-item">
              <NavLink to="/about" onClick={this.closeNav.bind(this)}>About the project</NavLink>
            </div>
            <div className="mobile-menu-sub-item">
              <NavLink to="/about" onClick={this.closeNav.bind(this)}><ArrowForwardIosIcon />Project Presentation</NavLink>
            </div>
            <div className="mobile-menu-sub-item">
              <NavLink to="/partners" onClick={this.closeNav.bind(this)}><ArrowForwardIosIcon />Partners</NavLink>
            </div>
            <div className="mobile-menu-item">
              <NavLink to="/activities" onClick={this.closeNav.bind(this)}>Activities</NavLink>
            </div>
            <div className="mobile-menu-item">
              <NavLink to="/artists" onClick={this.closeNav.bind(this)}>Our Artists</NavLink>
            </div>
            <div className="mobile-menu-item">
              <NavLink to="/news" onClick={this.closeNav.bind(this)}>News</NavLink>
            </div>

            <div className="mobile-menu-item">
              <div className="contact-button">
                <Button color="secondary" variant="contained" onClick={this.closeNav.bind(this)}>
                  <NavLink to="/calls" >Open Calls</NavLink>
                </Button>
              </div>
              <div className="contact-button">
                <Button color="primary" variant="contained" onClick={this.closeNav.bind(this)}>
                  <NavLink to="/contact">Get in touch!</NavLink>
                </Button>
              </div>
            </div>
            <div className="mobile-menu-item"> </div>
          </div>
        ) : (
          <div></div>
        )}

        <div
          className={
            this.state.scroll ? "navbar-container-scroll" : "navbar-container"
          }
        >
          <div className="left-navbar">
            <div className="item dropdown">
              <NavLink to="/about">About the project</NavLink>
              <div className="subitems">
                <div className="subitem"><NavLink to="/about">Project Presentation</NavLink></div>
                <div className="subitem">
                  <NavLink to="/partners">Partners</NavLink>
                </div>
              </div>
            </div>
            <div className="item">
              <NavLink to="/activities">Activities</NavLink>
            </div>

            <div className="item">
              <NavLink to="/artists">Our Artists</NavLink>
            </div>
            <div className="item">
              <NavLink to="/news">News</NavLink>
            </div>
          </div>

          <div className="center-logo">
            <div className={this.state.homepage ? "logo-hide" : "logo"}>
              <img
                src="https://i.ibb.co/kGvwLwn/ezgif-com-gif-maker-2.gif"
                alt=""
                onClick={this.toHomepage.bind(this)}
              />
            </div>
          </div>
          <div className="right-navbar">

            <div className="social-logos">
              <div className="">
                <a
                  href="https://www.facebook.com/TremoloProject" target="_blank" rel="noreferrer"
                >
                  <FacebookIcon fontSize="large"></FacebookIcon>
                </a>
              </div>
              <div className="">
                <a
                  href="https://www.instagram.com/tremolo.project/?fbclid=IwAR3NNr5VEC8y_z-UI2w0lSLio4lIzhhQjUnd-suFBD1tOVYtohHhxmABlA8"
                  target="_blank"
                  rel="noreferrer"
                >
                  <InstagramIcon fontSize="large"></InstagramIcon>
                </a>
              </div>
            </div>
            <div className="contact-button">
              <Button color="secondary" variant="contained">
                <NavLink to="/calls">Open Calls</NavLink>
              </Button>
            </div>
            <div className="contact-button">
              <Button color="primary" variant="contained">
                <NavLink to="/contact">Get in touch!</NavLink>
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Navbar);
