import React, { Component } from 'react'
import './partners.scss'
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import LanguageIcon from '@material-ui/icons/Language';
import ReactMarkdown from "react-markdown";
import YouTubeIcon from '@material-ui/icons/YouTube';

export default class Partner extends Component {
    constructor(props) {
        super(props)
        this.state = {
            partner: props.location.state.partner
        }
    }
    render() {
        return (
            <div className="partner-container">
                <div className="partner-header">
                    <div className="partner-name">
                        {this.state.partner.partner_name}
                        <div className="icons">
                            {
                                this.state.partner.facebookURL !== null ?
                                    <div className="icon">
                                        <a href={this.state.partner.facebookURL} rel="noreferrer" target="_blank"><FacebookIcon fontSize="large" /></a>
                                    </div>
                                    :
                                    <div></div>
                            }

                            {
                                this.state.partner.instagramURL !== null ?
                                    <div className="icon">
                                        <a href={this.state.partner.instagramURL} rel="noreferrer" target="_blank"><InstagramIcon fontSize="large" /></a>
                                    </div>
                                    :
                                    <div></div>
                            }

                            {
                                this.state.partner.websiteURL !== null ?
                                    <div className="icon">
                                        <a href={this.state.partner.websiteURL} target="_blank"><LanguageIcon fontSize="large" /></a>
                                    </div>
                                    :
                                    <div></div>
                            }
                             {
                                this.state.partner.youtubeURL !== undefined ?
                                    <div className="icon">
                                        <a href={this.state.partner.youtubeURL} rel="noreferrer" target="_blank"><YouTubeIcon fontSize="large" /></a>
                                    </div>
                                    :
                                    <div></div>
                            }
                        </div>
                    </div>
                    <div className="partner-logo">
                        <img
                            src={this.state.partner.url}
                            alt='url'
                        />
                    </div>
                </div>
                <div className="body">
                    <ReactMarkdown>
                    {this.state.partner.partner_description}
                    </ReactMarkdown>
                </div>
            </div>
        )
    }
}
