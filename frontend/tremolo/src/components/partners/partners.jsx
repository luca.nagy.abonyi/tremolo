import React, { Component } from 'react'
import './partners.scss';
import contentService from "../../services/contentService";
import Button from "@material-ui/core/Button";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { CircularProgress } from '@material-ui/core';

class Partners extends Component {
    constructor(props) {
        super(props)
        this.state = {
            partners: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await contentService.getPartners().then(response => {
            this.setState({
                partners: response.data
            })
        })
    }
    render() {
        if (this.state.partners.length > 0) {
            return (
                <div className='partners-container'>
                    <div className="title-container">
                        <div className="title">PARTNERS</div>
                    </div>
                    <div className="partners-list">
                        {
                            this.state.partners.map(partner =>
                                <div className="partner-item" key={partner.id}>
                                    <div className="partner-logo">
                                        <img
                                            src={partner.url}
                                            alt={partner.id}
                                        />
                                    </div>
                                    <div>
                                        <div className="partner-name">{partner.partner_name}</div>
                                        <div className="partner-desc">{partner.short_description}</div>
                                        <div className="button">
                                        <NavLink to={{pathname:"/partner/"+partner.partner_name, state: {partner}}} >
                                            <Button variant="contained" color="primary" className="button">Read More</Button>
                                        </NavLink>  
                                        </div>
                                    </div>
                                  
                                </div>
                            )
                        }

                    </div>
                </div>
            )
        } else {
            return (
                <div className='partners-container' style={{height: '100vh'}}>
                    <div className="title-container">
                        <div className="title">PARTNERS</div>
                    </div>
                    <CircularProgress size={100}/>
                </div>
            )
        }

    }
}
export default withRouter(Partners)