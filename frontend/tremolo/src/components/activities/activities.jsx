import React, { Component } from 'react'
import './activities.scss';
import { CircularProgress } from '@material-ui/core';
import contentService from "../../services/contentService";
import ReactMarkdown from "react-markdown";

export default class Activities extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activities: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await contentService.getActivities().then(response => {
            this.setState({
                activities: response.data
            })
        })
    }
    render() {
        if (this.state.activities.length > 0) {
            return (
                <div className="activities-container">
                    <div className="title-container">
                        <div className="title">ACTIVITIES</div>
                    </div>
                    <div className="activities-list">
                        {
                            this.state.activities.map(activity =>
                                <div className="activity-item" key={activity.id}>
                                    <div className="activtiy-title"><span>{activity.Title}</span></div>
                                    <div className="activity-desc"><ReactMarkdown>{activity.Description}</ReactMarkdown></div>
                                </div>
                            )
                        }
                    </div>
                </div>
            )
        } else {
            return (
                <div style={{ height: '100vh' }}>
                     <div className="title-container">
                        <div className="title">ACTIVITIES</div>
                    </div>
                    <CircularProgress size={100} />
                </div>
            )
        }

    }
}
