import React, { Component } from 'react'
import "./calls.scss";
import contentService from "../../services/contentService";
import Button from "@material-ui/core/Button";
import { NavLink } from "react-router-dom";
import { CircularProgress } from '@material-ui/core';
import TextTruncate from 'react-text-truncate';
export default class Calls extends Component {

    constructor(props) {
        super(props)
        this.state = {
            calls: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await contentService.getCalls().then(response => {
            this.setState({
                calls: response.data
            })
        })
    }
    render() {
        if (this.state.calls.length > 0) {
            return (
                <div className="news-container">
                    <div className="title">
                        <div className="title-container">
                            <div className="title">
                                OPEN CALLS
                            </div>
                        </div>
                    </div>
                    <div className="news-list">
                    {this.state.calls.map(call =>
                        <div className="news" key={call.id}>
                            <div className="news-title">
                                {call.Title}
                            </div>
                           
                            <div className="news-image">
                                <img src={call.Header} alt="" />
                            </div>
                            <div className="news-text">
                                <TextTruncate
                                    line={8}
                                    element="span"
                                    truncateText="…"
                                    text={call.Description}
                                />
                            </div>
                            <NavLink to={{ pathname: "/calls/" + call.Title, state: { call } }} >
                                <Button variant="contained" color="primary" className="button">Read More</Button>
                            </NavLink>
                        </div>
                    )}
                    </div>
                </div>
            )
        } else {
            return (
                <div className='partners-container' >
                    <div className="title-container">
                            <div className="title">
                                OPEN CALLS
                            </div>
                        </div>
{/*                     <CircularProgress size={100} />
 */}                </div>
            )
        }
    }
}
