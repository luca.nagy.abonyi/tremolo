import React, { Component } from 'react'
import './calls.scss'
import ReactMarkdown from "react-markdown";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { NavLink } from "react-router-dom";
import Button from "@material-ui/core/Button";
import LatestNews from '../news/latest-news';
export default class OneCall extends Component {

    render() {
        return (
            <div>
                <div className="one-news-container">
                    <div >
                        <NavLink to={'/calls'} className="back" >
                            <ArrowBackIcon />
                            <div>OPEN CALLS</div>
                        </NavLink>
                    </div>
                    <div className="one-news">
                        <div className="title">
                            {this.props.location.state.call.Title}
                        </div>
                        <div className="text">
                            <ReactMarkdown>
                                {this.props.location.state.call.Description}
                            </ReactMarkdown>
                        </div>
                        <div className="apply">
                            <Button color="secondary" variant="contained">
                            APPLY
                            </Button>
                        </div>
                    </div>
                </div>
                <LatestNews />
            </div>
        )


    }
}
