import React, { Component } from "react";
import "./contact.scss";
import contentService from "../../services/contentService";
import emailjs from 'emailjs-com';
const USER_ID = process.env.REACT_APP_USER_ID;
const PROJECT = process.env.REACT_APP_PROJECT;
const TEMPLATE = process.env.REACT_APP_TEMPLATE;

export default class Contact extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contacts: [],
      name: '',
      email: '',
      text: ''
    }
  }

  componentDidMount() {
    this.getData()
  }

  async getData() {
    await contentService.getContacts().then(response => {
      this.setState({
        contacts: response.data
      })
    })
  }

  handleName(e) {
    this.setState({
      name: e.target.value
    })
  }

  handleEmail(e) {
    this.setState({
      email: e.target.value
    })
  }

  handleText(e) {
    this.setState({
      text: e.target.value
    })
  }

  sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm(PROJECT, TEMPLATE, e.target, USER_ID)
      .then((result) => {
        alert('Email sent to Tremolo, thanks!')
      }, (error) => {
        console.log(error.text);
      });
  }

  render() {
    if (this.state.contacts.length > 0) {
      return (
        <div className="contact">
          <div className="contact-header">
            <div className="title">GET IN TOUCH!</div>
          </div>
          <div className="contact-body">
            <div className="emails">
              {
                this.state.contacts.map(contact =>
                  <div className="email" key={contact.id}>
                    {contact.contact_name}
                    <div className="email-address">
                      <a href={"mailto:" + contact.email}>
                        {contact.email}
                      </a>
                    </div>
                  </div>
                )
              }
            </div>
            <form className="form-container" onSubmit={this.sendEmail}>
              <input type="hidden" name="contact_number" className='input' />
              <label><span>Name</span></label>
              <input type="text" name="user_name" className='input' required/>
              <label><span>Email</span></label>
              <input type="email" name="user_email" className='input' required/>
              <label><span>Message</span></label>
              <textarea rows='4' name="message" required/>
              <div className="contact-button">
                <input type="submit" value="Send" className='send-button' />
              </div>
            </form>
          </div>
        </div>
      );
    } else {
      return (
        <div></div>
      )
    }

  }
}
