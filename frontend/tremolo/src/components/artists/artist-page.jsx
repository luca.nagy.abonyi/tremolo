import React, { Component } from 'react'
import './artists.scss'
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import LanguageIcon from '@material-ui/icons/Language';
import ReactMarkdown from "react-markdown";
import YouTubeIcon from '@material-ui/icons/YouTube';
import ImageGallery from 'react-image-gallery';

export default class ArtistPage extends Component {
    constructor(props) {
        super(props)
        let images = []
        let row = {
            original: props.location.state.picture_link,
        }

        images.push(row)

        this.state = {
            artist: props.location.state,
            images: images
        }
    }

    render() {

        return (
            <div className="artist-page-container">
                <div className="logos">
                    <div className="location">
                        <span>{this.state.artist.country}</span>
                    </div>
                    {
                        this.state.artist.facebookURL !== undefined ?
                            <div className="icon">
                                <a href={this.state.artist.facebookURL} target="_blank" rel="noreferrer">
                                    <FacebookIcon fontSize="large" />
                                </a>
                            </div>
                            :
                            <div></div>
                    }

                    {
                        this.state.artist.instagramURL !== undefined ?
                            <div className="icon">
                                <a href={this.state.artist.instagramURL} target="_blank" rel="noreferrer">
                                    <InstagramIcon fontSize="large" />
                                </a>
                            </div>
                            :
                            <div></div>
                    }

                    {
                        this.state.artist.spotifyURL !== undefined ?
                            <div className="icon">
                                <a href={this.state.artist.spotifyURL} target="_blank" rel="noreferrer">
                                    <i class="fab fa-spotify"></i>
                                </a>
                            </div>
                            :
                            <div></div>
                    }

                    {
                        this.state.artist.youtubeURL !== undefined ?
                            <div className="icon">
                                <a href={this.state.artist.youtubeURL} target="_blank" rel="noreferrer">
                                    <YouTubeIcon fontSize="large" />
                                </a>
                            </div>
                            :
                            <div></div>
                    }

                    {
                        this.state.artist.ITunesURL !== undefined ?
                            <div className="icon">
                                <a href={this.state.artist.iTunesURL} target="_blank" rel="noreferrer">
                                    <i class="fab fa-itunes"></i>
                                </a>
                            </div>
                            :
                            <div></div>
                    }

                    {
                        this.state.artist.WebsiteURL !== undefined ?
                            <div className="icon">
                                <a href={this.state.artist.websiteURL} target="_blank" rel="noreferrer">
                                    <LanguageIcon fontSize="large" />
                                </a>
                            </div>
                            :
                            <div></div>
                    }

                </div>
                <div>
                    <div className="artist-header">
                        <div>{this.state.artist.name}</div>
                    </div>
                    <div className="gallery">
                        <ImageGallery
                            showThumbnails={false}
                            showFullscreenButton={false}
                            showPlayButton={false}
                            showNav={false}
                            showBullets={true}
                            items={this.state.images} />
                    </div>
                    <div className="desc">
                    <ReactMarkdown>

                    {this.state.artist.short_description}
                    </ReactMarkdown>
                        <ReactMarkdown>
                            {this.state.artist.description}
                        </ReactMarkdown>
                    </div>
                    {
                        this.state.artist.video_link !== undefined ?
                            <iframe className="video" src={this.state.artist.video_link} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            :
                            <div></div>
                    }
                </div>
            </div>
        )
    }
}
