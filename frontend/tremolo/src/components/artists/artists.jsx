import React, { Component } from 'react'
import './artists.scss'
import contentService from "../../services/contentService";
import Button from "@material-ui/core/Button";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { CircularProgress } from '@material-ui/core';

class Artists extends Component {
    constructor(props) {
        super(props)
        this.state = {
            artists: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await contentService.getArtists().then(response => {
            this.setState({
                artists: response.data
            })
        })
    }
    toArtist(name, artist) {
        this.props.history.push({ pathname: 'artists/' + name, state: artist})

    }
    render() {
        if (this.state.artists.length > 0) {
            return (
                <div className="artists-container">
                    <div className="title-container">
                        <div className="title">ARTISTS</div>
                    </div>
                    <div className="artists-list">
                        {
                            this.state.artists.map(artist =>
                                <div className="artist-item" key={artist.id}>
                                    <div className="artist-img" style={{
                                        backgroundImage: `url('${artist.picture_link}')`,
                                        cursor: 'pointer'
                                    }}
                                    onClick={()=>this.toArtist(artist.name, artist)}>
                                    </div>
                                    <div>
                                        <div className="artist-name" onClick={()=>this.toArtist(artist.name, artist)}>{artist.name}</div>
                                        <div className="artist-desc">{artist.short_description}</div>
                                        <div className="button">
                                            <Button variant="contained" color="primary" className="button" onClick={() => this.toArtist(artist.name, artist)}>Read More</Button>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            )
        } else {
            return (
                <div style={{ height: '100vh' }} className="artists-container">
                    <div className="title-container">
                        <div className="title">ARTISTS</div>
                    </div>
                    <CircularProgress size={100} />
                </div>
            )
        }
    }
}
export default withRouter(Artists)