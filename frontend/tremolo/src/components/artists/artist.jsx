import React, { Component } from "react";
import "./artists.scss";
import { withRouter } from "react-router-dom";
class Arist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
      url: this.props.url,
      name: this.props.name
    };
  }

  setHover(value) {
    this.setState({
      hover: value,
    });
  }

  toArtist(name) {
    this.props.history.push({pathname: 'artists/'+name, state: this.props.artist})
  }

  render() {
    return (
      <div
        onMouseEnter={() => {
          this.setHover(true);
        }}
        onMouseLeave={() => {
          this.setHover(false);
        }}
        className="artist"
        style={
          this.state.hover
            ? {
              backgroundImage: `linear-gradient(0deg, rgba(158,34,46,0.6082808123249299) 0%, rgba(158,34,32,0.61) 100%), url('${this.state.url}')`,
              backgroundSize: "cover",
              backgroundPosition: "center top",
              transition: "all 0.2s ease-in",
              height: "350px",
              display: " flex",
              justifyContent: "center",
              alignItems: "center",
              flex: "2 0 50%"
            }
            : {
              backgroundImage: `linear-gradient(90deg, rgba(0,0,0,0.55) 0%, rgba(0,0,0,0.55) 55%), url('${this.state.url}')`,
              backgroundSize: "cover",
              backgroundPosition: "center",
              transition: "all 0.2s ease-in",
              height: "350px",
              display: " flex",
              justifyContent: "center",
              alignItems: "center",
              flex: "2 0 50%"
            }
        }
        onClick={()=>this.toArtist(this.state.name)}
      >
        <div className="artist-name">{this.state.name}</div>
      </div>
    );
  }
}


export default withRouter(Arist)
