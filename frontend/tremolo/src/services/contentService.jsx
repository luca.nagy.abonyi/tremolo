import axios from 'axios';
const SERVER_URL = process.env.REACT_APP_SERVER_URL;

class ContentService {
    
    getHomepage() {
        try {
            return axios.get(SERVER_URL+'/homepages')
        } catch (error) {
            console.log(error)
        }
    }

    getPartners(){
        try {
            return axios.get(SERVER_URL+'/partners')
        } catch (error) {
            console.log(error)
        }
    }

    getActivities(){
        try {
            return axios.get(SERVER_URL+'/activities')
        } catch (error) {
            console.log(error)
        }
    }

    getContacts(){
        try {
            return axios.get(SERVER_URL+'/contacts')
        } catch (error) {
            console.log(error)
        }
    }

    getAbout(){
        try {
            return axios.get(SERVER_URL+'/abouts')
        } catch (error) {
            console.log(error)
        }
    }

    getArtists(){
        try {
            return axios.get(SERVER_URL+'/artists')
        } catch (error) {
            console.log(error)
        }
    }

    getBlogs(){
         try {
            return axios.get(SERVER_URL+'/blogs')
        } catch (error) {
            console.log(error)
        }
    }

    getCalls(){
        try {
            return axios.get(SERVER_URL+'/open_calls')
        } catch (error) {
            console.log(error)
        }
    }

}


export default new ContentService();
