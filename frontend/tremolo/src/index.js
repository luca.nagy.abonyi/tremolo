import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#F4AD2C",
      main: "#F4AD2C",
      dark: "#F4AD2C",
      contrastText: "#fff",
    },
    secondary: {
      light: "#2E3F71",
      main: "#2E3F71",
      dark: "#2E3F71",
      contrastText: "#fff",
    },
    red: {
      light: "#9E222E",
      main: "#9E222E",
      dark: "#9E222E",
      contrastText: "#fff",
    }
  },
}); 
ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
            <App />
          </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
