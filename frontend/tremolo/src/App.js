import "./App.scss";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navbar from "./components/navbar/navbar";
import Homepage from "./components/homepage/homepage";
import About from "./components/about/about";
import Artists from "./components/artists/artists";
import Calls from "./components/calls/calls";
import Contact from "./components/contact/contact";
import News from "./components/news/news";
import Partners from "./components/partners/partners";
import Partner from "./components/partners/partner";
import Activities from './components/activities/activities';
import Footer from './components/footer/footer';
import ArtistPage from './components/artists/artist-page';
import OneNews from './components/news/one-news';
import React, { Component } from 'react'
import ScrollToTop from './ScrollToTop'
import OneCall from "./components/calls/call";

class App extends Component {
   componentDidUpdate(prevProps) {
    if (
      this.props.location.pathname !== prevProps.location.pathname
    ) {
     
      window.scrollTo(0, 0);
    }
  }

  render(){
  return (
    <div className="App">
      <Router>
        <ScrollToTop/>
        <Navbar></Navbar>
        <Route path="/" component={Homepage} exact></Route>
        <Route path="/about" component={About} exact></Route>
        <Route path="/artists" component={Artists} exact></Route>
        <Route path="/calls" component={Calls} exact></Route>
        <Route path="/contact" component={Contact} exact></Route>
        <Route path="/news" component={News} exact></Route>
        <Route path="/partners" component={Partners} exact></Route>
        <Route path="/activities" component={Activities} exact></Route>
        <Route path="/partner/:name" component={Partner} exact></Route>
        <Route path="/artists/:name" component={ArtistPage} exact></Route>
        <Route path="/news/:title" component={OneNews} exact></Route>
        <Route path="/calls/:title" component={OneCall} exact></Route>

        <Footer/>

      </Router>
    </div>
  );
  }
}

export default App;
